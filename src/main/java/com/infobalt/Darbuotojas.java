package com.infobalt;

public class Darbuotojas {
	
	private Integer id;
	private String vardas;
	private String pavarde;
	private String asmensKodas;
	private String pareigos;
	private String skyrius;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVardas() {
		return vardas;
	}
	public void setVardas(String vardas) {
		this.vardas = vardas;
	}
	public String getPavarde() {
		return pavarde;
	}
	public void setPavarde(String pavarde) {
		this.pavarde = pavarde;
	}
	public String getAsmensKodas() {
		return asmensKodas;
	}
	public void setAsmensKodas(String asmensKodas) {
		this.asmensKodas = asmensKodas;
	}
	public String getPareigos() {
		return pareigos;
	}
	public void setPareigos(String pareigos) {
		this.pareigos = pareigos;
	}
	public String getSkyrius() {
		return skyrius;
	}
	public void setSkyrius(String skyrius) {
		this.skyrius = skyrius;
	}

	

}

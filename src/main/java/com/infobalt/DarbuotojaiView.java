package com.infobalt;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public class DarbuotojaiView {
	
	private String notificationBar;

	private DarbuotojaiModel darbuotojaiModel;
	
	public void create() {
		darbuotojaiModel.setRedaguojamasDarbuotojas(new Darbuotojas());
	}

	public void update(Integer id) {
		darbuotojaiModel.setRedaguojamasDarbuotojas(darbuotojaiModel.searchDarbuotojasById(id));
	}

	public void delete(Integer id) {
		darbuotojaiModel.getDarbuotojai().remove(darbuotojaiModel.searchDarbuotojasById(id));
		darbuotojaiModel.setRedaguojamasDarbuotojas(null);
		zinute("Darbuotojas pasalintas");
	}

	public void cancel() {
		darbuotojaiModel.setRedaguojamasDarbuotojas(null);
	}

	public void save() {
		Integer id = darbuotojaiModel.getRedaguojamasDarbuotojas().getId();
		if (id != null) {
		} else {
			darbuotojaiModel.searchDarbuotojasById(id)
					.setVardas(darbuotojaiModel.getRedaguojamasDarbuotojas().getVardas());
			darbuotojaiModel.searchDarbuotojasById(id)
					.setPavarde(darbuotojaiModel.getRedaguojamasDarbuotojas().getPavarde());
			darbuotojaiModel.searchDarbuotojasById(id)
					.setAsmensKodas(darbuotojaiModel.getRedaguojamasDarbuotojas().getAsmensKodas());
			darbuotojaiModel.searchDarbuotojasById(id)
					.setPareigos(darbuotojaiModel.getRedaguojamasDarbuotojas().getPareigos());
			darbuotojaiModel.searchDarbuotojasById(id)
					.setSkyrius(darbuotojaiModel.getRedaguojamasDarbuotojas().getSkyrius());
			zinute("Darbuotojas atnaujintas");
			darbuotojaiModel.getRedaguojamasDarbuotojas().setId(darbuotojaiModel.getDarbuotojai().size());
			darbuotojaiModel.getDarbuotojai().add(darbuotojaiModel.getRedaguojamasDarbuotojas());
			zinute("Darbuotojas pridetas");
		}
		darbuotojaiModel.setRedaguojamasDarbuotojas(null);
	}

	protected void zinute(String zinute) {
		FacesMessage doneMessage = new FacesMessage(zinute);
		FacesContext.getCurrentInstance().addMessage(null, doneMessage);
	}

	public DarbuotojaiModel getDarbuotojaiModel() {
		return darbuotojaiModel;
	}

	public void setDarbuotojaiModel(DarbuotojaiModel darbuotojaiModel) {
		this.darbuotojaiModel = darbuotojaiModel;
	}
	
	public String cPage(){
		return "success";
	}
	
	public String javaPage(){
		return "success";
	}
	
	public String testavimoPage(){
		return "success";
	}
	
	public String backMygtukas(){
		return "success";
	}
	public String getNotificationBar() {

		return notificationBar;
	}

	public void setNotificationBar(String notificationBar) {
		this.notificationBar = notificationBar;
	}

	
}

package com.infobalt;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class DarbuotojaiModel {

	private List<Darbuotojas> darbuotojai = new ArrayList<Darbuotojas>();
	
	private Darbuotojas redaguojamasDarbuotojas;
	
	@PostConstruct public void postConstruct() { /* čia galim prikurti pradinių 

	duomenų ar nuskaityti juos iš DB */ }
	
	public Darbuotojas searchDarbuotojasById(Integer id) { 
		for(Darbuotojas list : darbuotojai){
			if(list.getId() == id){
				return list;
			}
		}
		return redaguojamasDarbuotojas;
		
	}

	public List<Darbuotojas> getDarbuotojai() {
		return darbuotojai;
	}

	public void setDarbuotojai(List<Darbuotojas> darbuotojai) {
		this.darbuotojai = darbuotojai;
	}

	public Darbuotojas getRedaguojamasDarbuotojas() {
		return redaguojamasDarbuotojas;
	}

	public void setRedaguojamasDarbuotojas(Darbuotojas redaguojamasDarbuotojas) {
		this.redaguojamasDarbuotojas = redaguojamasDarbuotojas;
	} 

	
	
}
